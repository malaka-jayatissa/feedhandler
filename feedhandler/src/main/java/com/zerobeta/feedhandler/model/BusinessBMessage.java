package com.zerobeta.feedhandler.model;
import com.zerobeta.common.model.SaleFeed;

public class BusinessBMessage {
    private String itemCode;
    private String purchaseTime;
    private float amount;
    private float discount;

    public void enrichSaleFeed(SaleFeed feed)
    {
        feed.setProductCode(this.itemCode);
        feed.setPosTransactionTime(this.purchaseTime);
        feed.setPosDiscount(this.discount);
        feed.setPosTransactionAmount(this.amount);

    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "BusinessBMessage [amount=" + amount + ", discount=" + discount + ", itemCode=" + itemCode
                + ", purchaseTime=" + purchaseTime + "]";
    }

}
