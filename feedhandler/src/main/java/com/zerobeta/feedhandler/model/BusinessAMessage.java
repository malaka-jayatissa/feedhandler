package com.zerobeta.feedhandler.model;


import java.text.SimpleDateFormat;
import java.util.Date;

import com.zerobeta.common.model.SaleFeed;

public class BusinessAMessage {

    private String skulid;
    private long transactionTime;
    private float transactionAmount;

    
    public void enrichSaleFeed(SaleFeed feed)
    {
        feed.setProductCode(this.skulid);
        Date resultdate = new Date(this.transactionTime);
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
        feed.setPosTransactionTime(format.format(resultdate));
        feed.setPosTransactionAmount(this.transactionAmount);
        
    }

    public String getSkulid() {
        return skulid;
    }

    public void setSkulid(String skulid) {
        this.skulid = skulid;
    }

    public long getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(long transactionTime) {
        this.transactionTime = transactionTime;
    }

    public float getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(float transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    @Override
    public String toString() {
        return "BusinessAMessage [skulid=" + skulid + ", transactionAmount=" + transactionAmount + ", transactionTime="
                + transactionTime + "]";
    }

}
