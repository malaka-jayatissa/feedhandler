package com.zerobeta.feedhandler.serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SalesFeedSerializer implements Serializer{
    private static final Logger logger = LoggerFactory.getLogger(SalesFeedSerializer.class);

    @Override
    public byte[] serialize(String topic, Object data) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(data);
            oos.close();
            byte[] b = baos.toByteArray();
            return b;
        }
        catch (IOException e) {
            logger.error("Serialization failed "+ e.getMessage());
            return new byte[0];
        }
       
    }

    
    @Override
    public void close() {
    }
    
}
