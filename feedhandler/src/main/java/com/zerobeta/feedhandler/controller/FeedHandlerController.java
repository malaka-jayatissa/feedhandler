package com.zerobeta.feedhandler.controller;

import com.zerobeta.feedhandler.model.BusinessAMessage;
import com.zerobeta.feedhandler.model.BusinessBMessage;
import com.zerobeta.common.model.SaleFeed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class FeedHandlerController {
    private static final Logger logger = LoggerFactory.getLogger(FeedHandlerController.class);
    @Autowired
    private KafkaTemplate<String, SaleFeed> kafkaTemplate;

    @MessageMapping("/business_a")
    public void messagefrombusinessA(BusinessAMessage message)
    {
        logger.info(message.toString());
        SaleFeed feed = new SaleFeed();
        message.enrichSaleFeed(feed);
        feed.setSequenceNumber(1);
        feed.setBusinessID("business_a");
        kafkaTemplate.send("sale_feed",feed);
        logger.info(feed.toString());
    }

    @MessageMapping("/business_b")
    public void messagefrombusinessB(BusinessBMessage message)
    {
        logger.info(message.toString());  
        SaleFeed feed = new SaleFeed();
        message.enrichSaleFeed(feed);
        feed.setSequenceNumber(1);
        feed.setBusinessID("business_b                                ");
        kafkaTemplate.send("sale_feed",feed);
        logger.info(feed.toString());
       
    }


}
